package edu.towson.cosc431.SINGH.todos

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.todo_fragment.view.*

class TodoFragment: Fragment(), IFragmentUpdate {

    override lateinit var controller: IFragmentController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.todo_fragment, container, false)

       // view .allView

        view.allView.setOnClickListener {
            controller.handleAllView()
        }

        view.activeView.setOnClickListener {
            controller.handleActiveView()

        }

        view.completedView.setOnClickListener {
            controller.handleCompletedView()
        }

        return view

    }
}