package edu.towson.cosc431.SINGH.todos

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

data class TodoModel (val title: String,
                      val contents:String,
                      val isComplete:Boolean,
                      val date: LocalDate)