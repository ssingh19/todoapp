package edu.towson.cosc431.SINGH.todos

interface IFragmentController {
    fun handleAllView()
    fun handleActiveView()
    fun handleCompletedView()
}